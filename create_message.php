<?php
    include("conn.php");
    $s = array();
    $l = checkInputs();
    if($l == 'done'){
        $message_text = $_POST['message_text'];
        $message_date = $_POST['message_date'];
        $message_owner_id = $_POST['message_owner_id'];
        $message_owner_email = $_POST['message_owner_email'];
        $chat_id = $_POST['chat_id'];
        createMessage($mysqli, $message_text, $message_date, $message_owner_id, $message_owner_email, $chat_id);
    }else{
        $s = array(
            'code'=>-1,
            'data'=>null,
            'message'=>$l
        );
        echo json_encode($s);
    }

    function checkInputs(){
        if(!isset($_POST['message_text'])){
            return 'You must write message text';
        }
        if(!isset($_POST['message_date'])){
            return 'You must pick message date';
        }
        return 'done';
    }

    function createMessage($mysqli, $message_text, $message_date, $message_owner_id, $message_owner_email, $chat_id){
        $stmt = $mysqli->prepare("insert into chat_messages(message_text,message_date,message_owner_id,message_owner_email,chat_id) values(?,?,?,?,?)");
        $stmt->bind_param('sssss',$message_text, $message_date, $message_owner_id, $message_owner_email, $chat_id);
        $stmt->execute();
        $result = $stmt->get_result();
        $id = $mysqli->insert_id;
        
        $mysqli->close();
        
        $s = array(
            'code'=>1,
            'data'=>array(
                'message_id'=>strval($id),
                'message_text'=>$message_text,
                'message_date'=>$message_date,
                'message_owner_id'=>$message_owner_id,
                'message_owner_email'=>$message_owner_email,   
                'chat_id'=>$chat_id   
            ),
            'message'=>'Message Added successfully'
        );
        echo json_encode($s);
    }
?>