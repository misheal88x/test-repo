<?php
    include("conn.php");
    $s = array();
    if(isset($_POST['email'])){
        $email = $_POST['email'];
        if(checkEmailIfExists($mysqli, $email)){
            $s = array(
                'code'=>-1,
                'data'=>null,
                'message'=>'Email already exists'
            );
            echo json_encode($s);
        }else{
            insertUser($mysqli, $email);
        } 
    }else{
        $s = array(
            'code'=>-1,
            'data'=>null,
            'message'=>'You must write email'
        );
        echo json_encode($s);
    }

    function checkEmailIfExists($mysqli, $email){
        $query = "SELECT * from user WHERE useremail='$email'";
        $result1 = $mysqli->query($query);
        $rows = array();
        while($row=mysqli_fetch_assoc($result1)){
            array_push($rows,$row);
        }
        if(count($rows) > 0){
            return true;
        }else{
            return false;
        }
    }

    function insertUser($mysqli, $email){
        $stmt = $mysqli->prepare("insert into user(useremail) values(?)");
        $stmt->bind_param('s',$email);
        $stmt->execute();
        $result2 = $stmt->get_result();
        $mysqli->close();
        $s = array(
            'code'=>1,
            'data'=>array(
                'email'=>$email
            ),
            'message'=>'User added successfully'
        );
        echo json_encode($s);
    }
    
?>