<?php
    include("conn.php");
    $s = array();
    $l = checkInputs();
    if($l == 'done'){
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $phone = $_POST['phone'];
        $faculity = $_POST['faculity'];
        if(checkIfEmailExists($mysqli,$email)){
            $s = array(
                'code'=>-1,
                'data'=>null,
                'message'=>'Email already exists'
            );
            echo json_encode($s);
        }else{
            insertVolunteer($mysqli, $name, $email, $password, $phone, $faculity);
        }
    }else{
        $s = array(
            'code'=>-1,
            'data'=>null,
            'message'=>$l
        );
        echo json_encode($s);
    }
    function checkInputs(){
        if(!isset($_POST['name'])){
            return 'You must write name';
        }
        if(!isset($_POST['email'])){
            return 'You must write email';
        }
        if(!isset($_POST['password'])){
            return 'You must write password';
        }
        if(!isset($_POST['phone'])){
            return 'You must write phone';
        }
        if(!isset($_POST['faculity'])){
            return 'You must write faculity';
        }
        return 'done';
    }
    function checkIfEmailExists($mysqli,$email){
        $query = "SELECT * from volunteers WHERE Email='$email'";
        $result1 = $mysqli->query($query);
        $rows = array();
        while($row=mysqli_fetch_assoc($result1)){
            array_push($rows,$row);
        }
        if(count($rows) > 0){
            return true;
        }else{
            return false;
        }
    }

    function insertVolunteer($mysqli, $name, $email, $password, $phone, $faculity){
        $stmt = $mysqli->prepare("insert into volunteers(VolunteerName,Email,Passworrd,phone,Faculty) values(?,?,?,?,?)");
        $stmt->bind_param('sssss',$name,$email,$password,$phone,$faculity);
        $stmt->execute();
        $result = $stmt->get_result();
        $id = $mysqli->insert_id;
        $mysqli->close();
        $s = array(
            'code'=>1,
            'data'=>array(
                'id'=>$id,
                'name'=>$name,
                'email'=>$email,
                'password'=>$password,
                'phone'=>$phone,
                'faculity'=>$faculity
            ),
            'message'=>'Volunteer Added successfully'
        );
        echo json_encode($s);
    }
    
?>