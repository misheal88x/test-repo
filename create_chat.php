<?php
    include("conn.php");
    $s = array();
    $l = checkInputs();
    if($l == 'done'){
        $date = $_POST['date'];
        $volunteer = $_POST['volunteer'];
        $user = $_POST['user'];
        $chat = checkChatIfExists($mysqli,$volunteer, $user);
        if($chat == null){
            createChat($mysqli, $date, $volunteer, $user);
        }else{
            $s = array(
                'code'=>-1,
                'data'=>$chat,
                'message'=>'Chat already created'
            );
            echo json_encode($s);
        }
    } else{
        $s = array(
            'code'=>-1,
            'data'=>null,
            'message'=>$l
        );
        echo json_encode($s);
    }

    function checkInputs(){
        if(!isset($_POST['date'])){
            return 'You must specify chat date';
        }
        if(!isset($_POST['volunteer'])){
            return 'You must specify the volunteer';
        }
        if(!isset($_POST['user'])){
            return 'You must specify the user';
        }
        return 'done';
    }

    function checkChatIfExists($mysqli, $volunteer, $user){
        $query = "SELECT * from chat WHERE VolunteerId=$volunteer AND useremail='$user'";
        $result1 = $mysqli->query($query);
        $rows = array();
        while($row=mysqli_fetch_assoc($result1)){
            array_push($rows,$row);
        }
        if(count($rows) > 0){
            return $rows[0];
        }else{
            return null;
        }
    }

    function createChat($mysqli, $date, $volunteer, $user){
    
        $stmt = $mysqli->prepare("insert into chat(ChatDate,VolunteerId,useremail) values(?,?,?)");
        $stmt->bind_param('sss',$date,$volunteer,$user);
        $stmt->execute();
        $result = $stmt->get_result();
        $id = $mysqli->insert_id;
        
        $mysqli->close();
        
        $s = array(
            'code'=>1,
            'data'=>array(
                'id'=>$id,
                'date'=>$date,
                'volunteer'=>$volunteer,
                'user'=>$user,   
            ),
            'message'=>'Chat Added successfully'
        );
        echo json_encode($s);
        
        
    }
    
?>